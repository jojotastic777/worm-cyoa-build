#+TITLE: Worm CYOA Build
#+AUTHOR: jojotastic777
#+OPTIONS: H:999

A response to [[https://cyoa.ltouroumov.ch/viewer/][Lt Ouroumov's Worm CYOA V17]].

I intend to construct this build in additive "layers", so as to be able to "branch off" from any given "layer".

Even though this isn't /technically/ a Jumpchain thing, it's enough of an evolutionary offshoot that I'm willing to borrow some of the terminology from Jumpchain to make thing easier for myself.

To that effect, the "target" of the CYOA, to whom the document refers in the second person, I will instead refer to as "Jumper" (as a sort of stand-in proper noun) or "The Jumper" (as a descriptor).

* Main Layer: Basic Self-Insert
:PROPERTIES:
:CUSTOM_ID: layer/basic-self-insert
:END:
Just a basic-bitch self-insert.

** Meta
- Target: You
  - Points:
    - =+5 SP=
- Awareness: Someone Else
  - Points:
    - =+5 CP=

** Difficulty
- Difficulty: Standard
- Starting Points: Normal
  - Points:
    - =+10 SP=
    - =+10 CP=

** Character
- Incarnation: Drop-In
  - =+5 SP=
  - =+5 CP=
- Gender: Same Gender

** Summary
Import Code:
#+BEGIN_SRC text
  8mhz,trog,dstd,eqhz,icne,cbnq
#+END_SRC

Total Points:
- =20 SP=
- =20 CP=

* Main Layer: Snarks-Like
:PROPERTIES:
:CUSTOM_ID: layer/snarks-like
:END:
Parent Layer: [[#layer/basic-self-insert][Basic Self-Insert]]

The goal here is to make a setting /generally inspired/ by how the Shards and Entities work in [[https://forums.sufficientvelocity.com/threads/mauling-snarks-worm-complete.41471/][Mauling Snarks]] rather than how they do in canon.

This means that, even if nobody (even the Jumper) actually knows it, the world isn't in danger of being Golden Morning'd. It /also/ involves several of the other "AU" elements of that fic, such as Shards generally having at least some semblance of personality to them (even if that's not really apparent to anyone who doesn't have Maul's power), Shards not having a "Conflict" drive so much as a "Usage" drive (and occasionally some other miscellaneous "drives"), as well as the whole situation regarding /how/ Earth Bet got into the situation it's in in the first place.

This /does not/ necessarily mean using /other/ elements of MS's worldbuilding, like the thing with the S9 being a black-ops/therapy group for people whose powers /require/ murder. Mostly it's the /less-obvious/ parts of MS's worldbuilding that I'm using here.

Additionally, I'm also going to make a couple of other changes, partially because the extra points are nice, but mostly just because I feel like it.

Also sets the Jumper's starting location, because that's under the "Setting" page, as well as the Jumper's starting /time/ because that fits in with the rest of the section.

** Scenario
- January 3rd, 2011
  - Points:
    - =+10 SP=
    - =+10 CP=
  - A perfectly reasonable starting point.
  - Sort of a shame that Jumper isn't going to be able to avert Taylor's trigger event, but IMO it's more likely to make an interesting story if he doesn't do so, and I don't want Jumper to be a /complete asshole/ so as to /choose/ not to avert Taylor's trigger event. It's much easier if the Jumper doesn't have a choice.

** Setting
- Setting: AU Earth Bet
  - This gives access to a bunch of really nice "world-alteration" options in the form of the "Continuity Modifiers", "Different World", "Different Cycle", and "Historical" sections.

*** Continuity Modifiers
- Natural Monsters
  - Points:
    - =+10 CP=
  - This is mostly a "because I feel like it" thing.
  - Also, the extra Character Points are a nice bonus.
- Benign
  - Points:
    - =-10 SP=
  - This is the "non-conflict drives" thing.
- Genocide is inefficient
  - Points:
    - =-30 CP=
  - This is the "shards/entities have MS-like motivations" thing.

*** Location
- Brockton Bay
  - Points:
    - =+5 CP=
  - The natural starting location, seeing as how it's where a huge portion of the "canon" story takes place.

** Summary
Import Code:
#+BEGIN_SRC text
  8mhz,trog,dstd,eqhz,9d7l,icne,cbnq,q6x83,i5x3,gy2s,9bvm,4tfy
#+END_SRC

Total Points:
- =30 SP=
- =5  CP=

* Main Layer: Faerie Weaknesses
:PROPERTIES:
:CUSTOM_ID: layer/faerie-weaknesses
:END:
Parent Layer: [[#layer/snarks-like][Snarks-Like]]

The goal of this layer is to give the Jumper a set of geasa and weaknesses that vaguely line up with the conventional "faerie" behaviors and weaknesses.

The reason for this is mostly because I think they're /neat,/ thematically, and the combination I use here actually gives a decent number of points.

I'll not give the Jumper the /full/ spread, of course, since I don't want my protagonist to be an /asshole/ like most fae are, but he's definitely going to get all the /interesting/ bits.

(I'm specifically not doing anything with "true names" here because dealing with that sort of thing is /too/ much of a pain in the ass for the points it'd give, I think.)

Geasa are worded in second person mostly because it feels easier (and possibly clearer?) to write them that way.

All geasa function based on the Jumper's preception of the world, but are /not/ able to be fooled by self-deception.

The consequences of breaking a "Conscious Geas" is /pain,/ with the severity and duration of that pain scaling with the severity of the breach. 

** Drawbacks
- Conscious Geas: "Do not lie /outright./"
  - Points:
    - =+3 SP=
    - =+8 CP=
  - Very much a fae-ish interperetation of "lying".
  - Presenting facts in a misleading way is allowed, as asking misleading /questions./
    - That is, the Jumper can use a collection of questions and/or technically-correct statements to /imply/ something untrue.
  - Lies by omission don't count as lies for the purposes of this Geas.
  - This Geas only applies to "explicit" communication such as speaking, writing, sign language, telepathy, etc.
    - It specifically /does not/ apply to things more along the line of /body language./
  - There is an exception worked into this Geas for telling explicitly-fictional stories.
    - So long as the fact that the story being told/written/whatever is fictional is made reasonably clear, the Jumper won't be "blamed" for misinterpereting things.
- Conscious Geas: "Do not break your given word."
  - Points:
    - =+3 SP=
    - =+8 CP=
  - Again, a very fae-ish interperetation of what counts as "breaking your word".
  - Specifically, Jumper is held to the /letter/ of agreements, rather than the spirit.
  - It's /very easy/ to get out of making an "agreement" for the purposes of this Geas.
    - The Jumper saying that he "would like to" do something doesn't constitute "giving his word" at all, and saying that he "currently intends to" do something leaves room for changing his mind later.
    - Adding /qualifiers/ also works, and generally uses the /most/ liberal interperetation of those qualifiers.
      - Jumper saying that he will do something "barring unforseen circumstances", for example, would let him skip out on doing whatever it is after encountering /any/ "unforseen circumstances", regardless of whether those circumstances have anything to do with the "promised" task.
  - This Geas /does not care/ about extenuating/unforseen circumstances. If the Jumper breaks his word, the consequences kick in regardless.
- Unconscious Geas: "Occasionally try to 'get away with things' and generally engage in mischief through the use of misleading language, omission of information, and 'loopholes'."
  - Points:
    - =+8 SP=
    - =+3 CP=
  - Specifically worded to avoid compelling the Jumper to do anything he would consider "evil", distateful, or even /mildly inconvenient./
    - Generally is intended to lean towards /lighthearted shenanigans/ rather than being one of the main reasons why the fae are capital-F *Feared*.
    - The "worst" this should /ever/ get is a bit of malicious complience when it would be /especially/ funny.
  - Generally is trying to get the Jumper to engage in the sort of "mischief" that Taylor does in Mauling Snarks, as well as the occasional bit of more "fae-ish" trickery.
- Unconscious Geas: "Occasionally point out 'loopholes' in instructions/orders you are given and in rule-sets you are subjected to."
  - Points:
    - =+8 SP=
    - =+3 CP=
  - Mostly intended to trigger on more "problematic" rules/instructions than the previous geas.
  - Intended to cause situations where the Jumper points some problematic wording out when it's relevant and then /doesn't act on it./
    - Sort of a "you know, /technically/ with the way you worded that I could interperet to mean [obviously incorrect meaning] instead of [intended meaning]" sort of thing.
- Allergic: "Cold Iron"
  - Points:
    - =+2 CP=
  - Specifically refers to reasonably-pure metallic iron (pure enough that you'd call it /iron/ and not an alloy of iron and something else) that has been shaped by human hands.
    - "Natural" iron, such as meteoric and telluric iron, have no effect at all.
    - The effect is stronger the more "involved" an acutal human is in the process.
      - This means that something produced entirely by a machine would have very little effect, while something produced by traditional blacksmithing would have a very /pronounced/ effect.
      - The effect is also really dependant on the iron being worked by /human/ hands.
        - An alient or, say, a /machine intelligence/ (\*cough*Dragon*cough*), could produce "worked" iron with no effect at all.
  - Instead of a conventional "allergic reaction", this instead produces /burns/ where a piece of "Cold Iron" touches the Jumper's skin. Additionally, wounds inflicted by "Cold Iron" (not just the burns, but also other "natural" injuries) heal unnaturally slowly and are moderately resistant to supernatural/parahuman healing.
  - I know that the "cold iron" business wasn't a /thing,/ historically, but I think it has the potential to be narriatively-interesting enough to bother with.
- Allergic: "Rowan Wood"
  - Points:
    - =+2 CP=
  - This is just a normal allergic reaction. Not a /coincidence,/ certainly, but it genuinely is just a normal, not-particularly-severe allergy with perfectly ordinary biological reasons for it.
    - In fact, it's actually specifically a particular chemical in the /sap/ that's the problem, rather than the wood itself.
  - The fact that it lines up with "Arbitrary Weakness" is /absolutely not/ a coincidence, though.
- Arbitrary Weakness
  - Points:
    - =+10 SP=
    - =+15 CP=
  - Sine both of them are kind of unreasonably specific, I'll do /two/ weaknesses:
    - First, the Jumper's powers-created effects (and magic) has trouble crossing boundaries delineated by a substance consisting of four-fifths finely-powdered (not granulated!) sodium chloride (that is, /salt/) and one-fifth rowan-wood ash.
      - The fact that the wood ash is produced from burning /rowan/ wood is important. Other kinds of wood won't work.
      - The exact quantity of substance needed to /block/ any given effect depends on the power of the effect in question.
      - The resulting mixture can, if desired, by added into a paint. This is markedly less effective than just putting a line of powder on the ground, but is significantly more convenient for permanent installations.
    - Additionally, the Jumper's powers get weaker in proximity to flames produced by burning rowan wood.
      - This lines up conveniently with the recipe for the /other/ weakness, which is intentional.

** Summary
Import Code:
#+BEGIN_SRC text
  8mhz,trog,dstd,eqhz,9d7l,icne,cbnq,wu6i/ON#2,1ro4/ON#2,4e20/ON#2,tq7u,q6x83,i5x3,gy2s,9bvm,4tfy
#+END_SRC

Total Points:
- =62 SP=
- =46 CP=

* Main Layer: Summoned Weirdo
:PROPERTIES:
:CUSTOM_ID: layer/summoned-weirdo
:END:

The Jumper being "summoned" is as good an excuse as any for why he's on Earth Bet, the fact that the summoning is accomplished by unusual means is a good excuse for why his /powers/ are unusual, and the summoning being /extremely public/ is a good excuse to get the Jumper a legal identity on Earth Bet.

Plus, that's all a /very/ good excuse for the PRT to desperately want to recruit Jumper. After all, he's /very/ obviously not from any known Earth and has demonstrated some highly-obvious "parahuman" abilities.

Now that I think about it, it's also a good excuse to get some /other/ groups on the Jumper's metaphorical ass...

Hmm, let's say that the Jumper was summoned by /the Fallen,/ not the mind-control branch but one of the others, in an attempt to get the next endbringer "early". This means that they're not particularly inclined to /kill/ the Jumper, but... Well. They're /Endbringer worshippers,/ so torturing the Jumper to the point that he "recieves enlightenment and assumes his true mantle" or whatever isn't entirely out of the question, I don't think.

Why they are doing this in /Brockton Bay/ isn't... important, I guess? Maybe there's something in the folklore surrounding the bay that made the idiots think it the best place for their ritual or some such bullshit.

** Drawbacks
- Wanted (Global): PRT
  - Points:
    - =+10 CP=
- Sick
  - Points:
    - =+10 CP=
  - "Summoning Sickness" is a classic trope that actually suits my services pretty well, all things considered.
- Fallen Worship
  - Points:
    - =+5  SP=
    - =+10 CP=
- The Summoning
  - Points:
    - =+5 CP=
    - =+5 SP=
- Extremely Obvious Summoning
  - Points:
    - =+5 CP=
    - =+5 SP=
  - The [[https://cyoa.ltouroumov.ch/images/v17/obj-nqd4.webp][image]] for this option in the CYOA is, in my opinion, quite funny.
- Worst Day Ever: "Bad Day"
  - Points:
    - =+20 SP=
    - =+20 CP=
  - Jumper was summoned /by the Fallen./ Even if they don't presently want to kill him, he's still going to have to /run like hell/ to get out of the situation unscathed.
  - Since Jumper /hasn't/ actually picked this for himself, he /is/ garunteed to get out of the initial situation alive. There wouldn't really be a /story/ if he died immediately, after all.
    
** Summary
Import Code:
#+BEGIN_SRC text
  8mhz,trog,dstd,eqhz,9d7l,icne,cbnq,wu6i/ON#2,1ro4/ON#2,4e20/ON#2,txtt/ON#1,yqih,6o4j,tq7u,gt5p,nqd4,08kd,4tg6,q6x83,i5x3,gy2s,9bvm,4tfy
#+END_SRC

Total Points:
- =97  SP=
- =106 CP=

* Main Layer: Inhuman Appearance
:PROPERTIES:
:CUSTOM_ID: layer/inhuman-appearance
:END:
Parent Layer: [[#layer/summoned-weirdo][Summoned Wierdo]]

Being obviously inhuman fits with the whole "summoned by culties" thing, and honestly probably makes it /easier/ to get a legal identity.

Having a /childlike/ appearance works well as a compounding factor to make the PRT/Protectorate /really/ want to get the Jumper under their authority. In /addition/ to that, though, the difference between the Jumper's /mental/ age and their /really obvious/ physical age makes for a potentially-interesting source of both internal /and/ external conflict.

Additionally, I think it would be a fairly interesting idea to make it so that the Jumper /can't/ make a distinction between their civilian and "cape" identities, to the point of /strongly resisting/ the idea of being given a "cape name". This goes /significantly/ against the currently-existing "cape culture" which is pretty firmly incessant on the importance of the civilian/cape identity distinction.

This will... probably result in something like what happened in [[https://forums.spacebattles.com/threads/constellations-worm-okami.414320/][Constellations]], where Taylor was eventually assigned the "cape name" Brushstroke, which a lot of people take really seriously but /Taylor herself/ treats kind of like it's a weird nickname or something.

** TODO Design an actual "Case 53"-style inhuman appearance.

** Character
- Appearance: Average

** Drawbacks
- Case 53
  - Points:
    - =+5  SP=
    - =+10 CP=
- Pint Sized
  - Points:
    - =+5  SP=
    - =+10 CP=
- Unconscious Geas: "Do not willingly create a distinction between your 'civilian' and 'cape' indentities."
  - Points:
    - =+8 SP=
    - =+3 CP=
  - Is strong enough that the Jumper can barely be convinced to /accept a "Cape Name",/ let alone make any real attempt to conceal his identity.
  - This is somewhat contributed to by the fact that the Jumper has such an inhuman appearance that any attempt at concealing his identity would be paper-thin /at best./
  - Jumper is /vehemently/ against using a "Cape"-style name as his /only/ name, as most "Case 53" individuals like Weld do.

** Summary
Import Code:
#+BEGIN_SRC text
  8mhz,trog,dstd,eqhz,9d7l,icne,cbnq,gl7t,13z7,jtyh,wu6i/ON#2,1ro4/ON#3,4e20/ON#2,txtt/ON#1,yqih,6o4j,tq7u,gt5p,nqd4,08kd,4tg6,q6x83,i5x3,gy2s,9bvm,4tfy
#+END_SRC

Total Points:
- =115 SP=
- =129 CP=

* Main Layer: Foundational Powers
:PROPERTIES:
:CUSTOM_ID: layer/foundational-powers
:END:
Parent Layer: [[#layer/inhuman-appearance][Inhuman Appearance]]

The foundation of the Jumper's powerset is a "Gamer System", which gives the Jumper powers based on a sort of generic amalgamation of Role Playing Games which is designed to fit whatever story the author intends to use the system to tell.

** Gamer System
The "Gamer System" is divided into a number of subsystems for ease-of-comprehension.

*** Subsystems
Due to Drawbacks, all "Gamer System" subsystems aside from the following are disabled by default:
- [[#gamer-system/subsystem/interface][Interface]]
- [[#gamer-system/subsystem/questing][Questing]]

The /very first/ Quest will unlock the [[#gamer-system/subsystem/perk-tree][Perk Tree]] subsystem, and most/all additional subsystems will be unlocked from there.

**** Interface
:PROPERTIES:
:CUSTOM_ID: gamer-system/subsystem/interface
:END:
The "Interface" subsystem provides the Gamer System with the ability to interact with the user though polysensory "interface elements". By default, these interface elements take the form of the classic "blue boxes" that are ubiquitous in LitRPG fiction. They /can,/ however, be customized to be anything that the user can imagine.

Sort of like how Rain can reconfigure /his/ interface in [[https://www.royalroad.com/fiction/25225/delve][Delve]], but without the ability to make macros. (That's a different subsystem.)

**** Macros
:PROPERTIES:
:CUSTOM_ID: gamer-system/subsystem/macros
:END:
Dependencies:
- [[#gamer-system/subsystem/interface][Interface]]

It is possible for the user, with a significant expendature of cognative effort, to "program" the Gamer System to interact with the [[#gamer-system/subsystem/interface][Interface]] in a pre-defined way when prompted either by use of the [[#gamer-system/subsystem/interface][Interface]]. Macros can also create /new/ [[#gamer-system/subsystem/interface][Interface]] elements, as well as being able to delete only those [[#gamer-system/subsystem/interface][Interface]] elements which were also /created/ by this subsystem.

The macro-programming capabilities of this subsystem are somewhat crude by default, but are nevertheless turing complete.

This is basically the ability to make macros like Rain does in [[https://www.royalroad.com/fiction/25225/delve][Delve]].

(Becomes significantly more useful when used with the [[#gamer-system/subsystem/notebook][Notebook]].)

***** TODO Move the "Macros" subsystem to the relevant section when a layer introduces the "Thinker (Multitask)", "Thinker/Master (Autopilot)", and "Thinker/Master (Organized Mind)" powers.

**** Notebook
:PROPERTIES:
:CUSTOM_ID: gamer-system/subsystem/notebook
:END:
Dependencies:
- [[#gamer-system/subsystem/interface][Interface]]

This subsystem allows the user to create [[#gamer-system/subsystem/interface][Interface]] elements which contain information, allowing data to be stored persistently inside the Gamer System.

This is most commonly used to create something like a "Notebook" menu (thus the name of the Subsystem) which contains text and (sometimes) images, but is certainly not limited to that.

(Becomes /much/ more useful when used with [[#gamer-system/subsystem/macros][Macros]].)

***** TODO Move the "Notebook" subsystem to the relevant section when a layer introduces the "Noctis Cape" perk.

**** Questing
:PROPERTIES:
:CUSTOM_ID: gamer-system/subsystem/questing
:END:
Dependencies:
- [[#gamer-system/subsystem/interface][Interface]]

When the user is faced with a task that is likely to take notable personal effort to complete, this subsystem creates a Quest (in the way you might expect from reading many "Gamer" or "LitRPG" stories) and presents it to the user by way of the [[#gamer-system/subsystem/interface][Interface]]. For particularly lengthly multi-step endeavors, this subsystem might create a Quest /Chain,/ possisbly a /branching/ Quest Chain if that's relevant.

Quests can have multiple methods of completing them, with different rewards for each method of completion.

Some Quests can be "hidden", which means that the user can't see a Quest using their interface until some condition is met. "Achievements" as they're used under some other systems are implemented this way.

Quests can have several different kinds of rewards.
- Most commonly, Quests will reward [[#gamer-system/subsystem/experience][Experience]].
  - If the required subsystem is available, Quests will almost /always/ reward at least some [[#gamer-system/subsystem/experience][Experience]].
- Sometimes, Quests will directly award [[#gamer-system/subsystem/attributes][Attribute Points]].
  - This is usually a result of completing some great feat tied primarily to a specific [[#gamer-system/subsystem/attributes][Attribute]].
- Quests will occasionally award /physical items,/ though this is fairly uncommon.
  - The most common example of this is a Quest to protect or destroy a specific /existing/ item giving the user a /copy/ of that item upon completion, though this is certainly not the /only/ time Quests will reward items.
  - As a quirk of the system, Quests will /never/ award items if the Inventory subsystem is unavailable.
- Quests will /very infrequently/ directly award [[#gamer-system/subsystem/perk-tree][Perks]] and/or [[#gamer-system/subsystem/skill-tree][Skills]] to the user.
- Quests will /almost/ never directly unlock new subsystems.
  - Emphasis on /almost./

***** TODO Create link to inventory subsystem once I have a place for that.

**** Experience
:PROPERTIES:
:CUSTOM_ID: gamer-system/subsystem/experience
:END:
Dependencies:
- [[#gamer-system/subsystem/interface][Interface]]

While this subsystem isn't terribly useful on it's own, Experience (also sometimes shortened to XP) is the primary currency of the Gamer System as a whole and is used in a /number/ of other Subsystems.

Notably, this system /does not/ use the concept of "Levels" found in many other "Gamer" / "LitRPG" systems. Instead, subsystems which rely on Experience are expected to use a point-buy system.

Experience usually earned in one of two ways:
- The "basic" way to earn Experience is to simply /accomplish things./ It can be anything, really, so long as it takes more than a "nominal" amount of effort.
  - Here are a few examples of things that can earn Experience:
    - Defeating an enemy. (Duh)
      - This doesn't necessarily mean /killing/ an enemy.
    - Crafting an item.
      - Doesn't necessarily have to be a /unique/ item, but crafting nearly-identical items gives diminishing returns.
    - Successfully negotiating with another person.
- The other primary way to earn Experience is to recieve it as a reward from a [[#gamer-system/subsystem/questing][Quest]].

***** TODO Come up with some other ways to earn Experience.

**** Attributes
:PROPERTIES:
:CUSTOM_ID: gamer-system/subsystem/attributes
:END:
Dependencies:
- [[#gamer-system/subsystem/interface][Interface]]
- [[#gamer-system/subsystem/experience][Experience]]

Attributes work like in most other "Gamer" / "LitRPG" systems, except that instead of earning increased Attributes per level or earning "Attribute Points" each level, this subsystem does away with the concept of "Levels" entirely, instead allowing the user to purchase increased Attributes /directly using [[#gamer-system/subsystem/experience][Experience]]./

Additionally, Attributes translate to "real-world" applications somewhat differently from how most similar systems do. Rather than the Gamer System using Attributes to describe the /actual state of your body/ they instead provide a multiplicative boost to your existing attributes. A "strength" attribute, for example, would grant the user supernatural strength which multiplies their "mundane" or normal strength by the value of the user's "strength" attribute and by some arbitrary per-attribute factor which moderates the "effect" of a given attribute.

This subsystem doesn't provide any specific attributes by itself. It is left to dependant subsystems to implement their own attributes on an as-needed basis.

***** TODO Explain the "arbitrary per-attribute factor" better.
***** TODO Honestly, I should probably revamp this /entire subsystem/. It's.. sort of in a "minimum viable product" state.

**** Perk Tree
:PROPERTIES:
:CUSTOM_ID: gamer-system/subsystem/perk-tree
:END:
Dependencies:
- [[#gamer-system/subsystem/interface][Interface]]
- [[#gamer-system/subsystem/experience][Experience]]

This is intended as a "perk tree" vaguely remeniscent of [[https://www.pathofexile.com/][Path of Exile]]'s [[https://www.pathofexile.com/passive-skill-tree][Passive Skill Tree]], except literally infinite in all directions. And possibly with multiple purchasable "root" nodes? I'm not entirely sure, honestly.

Mostly, this is meant to have "Perks" which expand the user's capabilities in some new way which they wouldn't have been able to do without buying a "Perk". "Perks" should be acquired by spending [[#gamer-system/subsystem/experience][Experience]], and should be very expensive.

***** TODO This subsystem needs a /significantly/ more in-depth explaination.

**** Skill Tree
:PROPERTIES:
:CUSTOM_ID: gamer-system/subsystem/skill-tree
:END:
Dependencies:
- [[#gamer-system/subsystem/interface][Interface]]
- [[#gamer-system/subsystem/experience][Experience]]

This is basically the same thing as the [[#gamer-system/subsystem/perk-tree][Perk Tree]], but instead of entirely new capabilities it should allow the user to "shortcut" learning about existing capabilities. It should also contain "mundane" skills of all kinds, as well as including the ability to purchase knowledge about technology. Sort of like how the "Inspired Inventor" power that I've seen in a lot of stories, but each individual purchase is /way way way/ more specific, and it's not even /primarily/ focused on technology.

More simply, you should be able to purchase knowledge of things you can /technically/ already do without gaining any entirely-new capabilities, no matter how skilled you need to be to do those things.

For example, if you can do magic, this should let you purchase knowledge of how to do /specific spells,/ and also knowledge about how to make your /own/ spells.

Really, it might be smarter to just integrate all of the things purchasable here as "Mundane Skill" nodes on the [[#gamer-system/subsystem/perk-tree][Perk Tree]]? Not entirely sure, TBH.

***** TODO Explain this better.

** Powers Origin
- Shardless
  - Points:
    - =-10 SP=
    - =-10 CP=

** Powers
*** Shardless Powers
**** Foundation Powers
- Gamer System
  - Points:
    - =-20 SP=
- Dynamist Jr.
  - Points:
    - =-20 SP=
- Skill Tree System
  - Points:
    - =-8 SP=
  - Provides the [[#gamer-system/subsystem/skill-tree][Skill Tree]] subsystem.

***** Upgrades
- Systematic Progression Systems
  - Points:
    - =+20 SP=
- Skill Tree Jr.
  - Somewhat confusingly, provides the [[#gamer-system/subsystem/perk-tree][Perk Tree]] subsystem.

** Drawbacks
- Second Trigger
  - Points:
    - =+10 SP=
    - =+10 CP=
- Acclimation
  - Points:
    - =+15 SP=
    - =+15 CP=
- Jet Lag
  - Points:
    - =+10 SP=
    - =+10 CP=
  - Makes everything on the Perk Tree more expensive.
- Snail's Pace
  - Points:
    - =+20 SP=
    - =+20 CP=
  - Makes everything on the Perk Tree /even more/ expensive.
- Starting From Nothing
  - Points:
    - =+20 SP=
    - =+20 CP=
      
** Summary

*** TODO Draw the rest of the fucking owl.

